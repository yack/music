#!/usr/bin/env python
# -*- coding: utf-8 -*-

# Music -- An HTML5 music player
# By: Cyril Lacoux <clacoux@ifeelgood.org>
#
# Copyright (C) 2015 Cyril Lacoux
# http://ifeelgood.org/music
#
# This file is part of Music.
#
# Music is free software; you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# Music is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import os

from setuptools import setup, find_packages

here = os.path.abspath(os.path.dirname(__file__))
with open(os.path.join(here, 'README.md')) as f:
    README = f.read()
with open(os.path.join(here, 'CHANGES.md')) as f:
    CHANGES = f.read()

requires = [
    'pyramid',
    'pyramid_mako',
    'pyramid_tm',
    'SQLAlchemy',
    'transaction',
    'zope.sqlalchemy',
    'waitress',
]

setup(
    name='Music',
    version='0.0',
    description='Music',
    long_description=README + '\n\n' + CHANGES,
    classifiers=[
        "Programming Language :: Python",
        "Framework :: Pyramid",
        "Topic :: Internet :: WWW/HTTP",
        "Topic :: Internet :: WWW/HTTP :: WSGI :: Application",
    ],
    author='Cyril Lacoux',
    author_email='clacoux@ifeelgood.org',
    url='http://ifeelgood.org/music',
    keywords='web wsgi bfg pylons pyramid',
    packages=find_packages(),
    include_package_data=True,
    zip_safe=False,
    test_suite='music',
    install_requires=requires,
    entry_points="""\
    [paste.app_factory]
    main = music:main
    [console_scripts]
    initialize_Music_db = music.scripts.initializedb:main
    """,
)
