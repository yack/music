# -*- coding: utf-8 -*-

# Music -- An HTML5 music player
# By: Cyril Lacoux <clacoux@ifeelgood.org>
#
# Copyright (C) 2015 Cyril Lacoux
# http://ifeelgood.org/music
#
# This file is part of Music.
#
# Music is free software; you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# Music is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

""" Pyramid initialization """

from pyramid.config import Configurator
from sqlalchemy import engine_from_config

from music.models import Base
from music.models import DBSession


def main(global_config, **settings):
    """ This function returns a Pyramid WSGI application. """

    engine = engine_from_config(settings, 'sqlalchemy.')
    DBSession.configure(bind=engine)
    Base.metadata.bind = engine

    config = Configurator(settings=settings)
    config.include('pyramid_mako')

    # Session setup
    config.include('pyramid_beaker')

    # I18n setup
    config.include('pyramid_helpers.i18n')
    # config.add_translation_dirs('music:locale/')

    # Forms setup
    config.include('pyramid_helpers.forms')

    # Pagers setup
    config.include('pyramid_helpers.paginate')

    # Replace JSON renderer (callback support)
    config.add_renderer('json', 'pyramid_helpers.renderers.json_renderer_factory')

    # Static view
    config.add_static_view('static', 'static', cache_max_age=3600)

    # Front views
    config.add_route('index', '/')
    config.add_route('albums', '/albums')
    config.add_route('artists', '/artists')
    config.add_route('folders', '/folders')
    config.add_route('player', '/player')
    config.add_route('playlists', '/playlists')
    config.add_route('library', '/library/*subpath')

    # API views
    config.add_route('api.albums', '/api/1.0/albums')
    config.add_route('api.artists', '/api/1.0/artists')
    config.add_route('api.folders', '/api/1.0/folders')
    config.add_route('api.playlist', '/api/1.0/playlist')
    config.add_route('api.playlists', '/api/1.0/playlists')
    config.add_route('api.playlists-get', '/api/1.0/playlists/:playlist', request_method='GET')
    config.add_route('api.playlists-put', '/api/1.0/playlists', request_method='PUT')
    config.add_route('api.playlists-post', '/api/1.0/playlists/:playlist', request_method='POST')

    config.scan('music.views')
    return config.make_wsgi_app()
