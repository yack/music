# -*- coding: utf-8 -*-

# Music -- An HTML5 music player
# By: Cyril Lacoux <clacoux@ifeelgood.org>
#
# Copyright (C) 2015 Cyril Lacoux
# http://ifeelgood.org/music
#
# This file is part of Music.
#
# Music is free software; you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# Music is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.


import formencode
from formencode import validators
import logging

log = logging.getLogger(__name__)


class FolderForm(formencode.Schema):
    allow_extra_fields = True
    filter_extra_fields = True

    path = validators.String(if_missing=None, if_empty=None)


class PlaylistForm(formencode.Schema):
    allow_extra_fields = True
    filter_extra_fields = True

    items = formencode.ForEach(validators.String())
