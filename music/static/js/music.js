/*
 * Music -- An HTML5 music player
 * By: Cyril Lacoux <clacoux@ifeelgood.org>
 *
 * Copyright (C) 2015 Cyril Lacoux
 * http://ifeelgood.org/music
 *
 * This file is part of Music.
 *
 * Music is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * Music is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

var l = (function() {
    // Global variables
    var options;
    var page;

    var listBlock;
    var apiUrl;

    function init(p_, o_) {
        options = o_;
        page = p_;

        apiUrl = $$(page.container).find('.list-block.library').data('url');
        listBlock = $$(page.container).find('.list-block.library > ul');

        if (page.name == 'player') {
            // Player page
            $$('.btn-clear-all').hide();
            $$('.btn-clear-all').on('click', function(e) {
                // Clear the playlist
                p.stop();

                // FIXME: display any error
                $$.ajax({
                    url: options.playlist,
                    method: 'DELETE',
                    success: function(data) { if (data && data.success) { refresh(); } }
                });
            });

        }
        else {
            // Other pages
            $$('.btn-play-all').hide();
            $$('.btn-play-all').on('click', function(e) {
                // Add all items to playlist
                var items = Array();
                listBlock.find('a.music').each(function(index, element) {
                    items.push($$(element).data('path'));
                });

                $$.post(options.playlist, {items: items}, function(data) {});

                // TODO/FIXME : cancel event propagation on failure (ie: don't got to player
                return false;
            });
        }
    }

    function refresh(callback) {
        if (!apiUrl) {
            return false;
        }

        if (page.query.path) {
            var params = {path: decodeURI(page.query.path)};
        }
        else {
            var params = null;
        }

        // Clear list
        listBlock.html('');

        // Hide global action button
        if (page.name == 'player') {
            // Player page
            $$('.btn-clear-all').hide();
        }
        else {
            // Other pages
            $$('.btn-play-all').hide();
        }

        $$.getJSON(apiUrl, params, function(data) {
            var html;
            var item;
            var path;
            var hasMusic = false;
            if (data && data.success) {
                // Add items to list
                for (var i=0; i<data.items.length; i++) {
                    item = data.items[i];
                    item.index = i;
                    if (item.type == 'music') {
                        hasMusic = true;
                        item.link = '/player';
                    }
                    else {
                        if (item.path) {
                            path = item.path + '/' + item.name;
                        }
                        else {
                            path = item.name;
                        }
                        item.path = path;
                        item.link = sprintf('/%s?path=%s', page.name, path);
                    }

                    html = Template7.templates.libraryItemTemplate(item);
                    listBlock.append(html);
                }

                if (hasMusic) {
                    if (page.name == 'player') {
                        // Player page
                        $$('.btn-clear-all').show();

                        listBlock.find('a.music').on('click', function() {
                            // Play the file
                            var index = parseInt($$(this).data('index'), 10);
                            p.play(index);
                        });
                    } else {
                        // Other pages
                        $$('.btn-play-all').show();

                        listBlock.find('a.music').on('click', function() {
                            // Add the file to playlist
                            var path = $$(this).data('path');
                            var items = Array(path);
                            $$.post(options.playlist, {items: items}, function(data) {});
                        });
                    }
                }
            }

            if (callback) {
                callback();
            }
        });
    }

    return {
        init: init,
        refresh: refresh
    };
}());


var p = (function() {

    // Global variables
    var currentCodec;
    var currentTrack;
    var options;
    var listBlock;
    var page;
    var player;
    var playerSlider;

    function init(p_, o_) {
        options = o_;
        page = p_;

        $$('.control-prev').on('click', function(e) {
            e.preventDefault();

            prev();
        });

        $$('.control-stop').on('click', function(e) {
            e.preventDefault();

            stop(true);
        });

        $$('.control-play').on('click', function(e) {
            e.preventDefault();

            togglePlayback();
        });

        $$('.control-next').on('click', function(e) {
            e.preventDefault();

            next();
        });

        listBlock = $$(page.container).find('.list-block.library > ul');

        currentTrack = 0;
    }

    /*
     * Load player and apropriate codec for extension
     */
    function load(extension, callback) {
        if (!(extension in options.codecs)) {
            console.log('FIXME: ERROR');
            return;
        }

        if (currentCodec === extension) {
            // Already loaded
            callback();
            return;
        }

        function loadJS(src, callback) {
            var tag = document.createElement('script');
            tag.src = src
            tag.onload = callback;
            document.body.appendChild(tag);
        };

        // (re)load audio player and codec
        loadJS(options.main, function() {
            loadJS(options.codecs[extension], function() {
                currentCodec = extension;
                callback();
            });
        });
    }

    function play(index) {
        if (!options.library) {
            console.log('FIXME: Missing library!');
            return;
        }

        if (typeof(index) == 'undefined' && player) {
            return;
        }

        stop();

        if (!index || index >= listBlock.find('a.music').length) {
            index = 0;
        }
        currentTrack = index;

        var element = $$(listBlock.find('a.music')[currentTrack]);
        if (!element.length) {
            console.log('FIXME: Invalid index!');
            return;
        }

        $$('.toolbar').removeClass('toolbar-hidden');
        element.find('i.fa-music').removeClass('fa-music').addClass('fa-play');

        var extension = element.data('extension');
        var path = element.data('path');
        var name = element.data('name');
        var fullpath = sprintf('%s%s', options.library, path);  // library ends with /

        load(extension, function() {
            player = AV.Player.fromURL(fullpath);

            player.on('duration', function(value) {
                $$('.player-slider').data('max', value);
                $$('.player-slider').css({width: 0});
                $$('.elapsed-time').html('00:00');
                $$('.current-track').html(name);
            });

            player.on('progress', function(value) {
                var m;
                var s = value / 1000;
                var hr = Array();

                // Minutes
                m = Math.floor(s / 60);
                hr.push(sprintf('%02d', m));
                s = s % 60;

                // Seconds
                hr.push(sprintf('%02d', s));

                var duration = $$('.player-slider').data('max');
                $$('.player-slider').css({width: sprintf('%.2f%%', (value * 100 / duration))});
                $$('.elapsed-time').html(hr.join(':'));
            });

            player.on('end', function() {
                next();
            });

            player.preload();

            player.on('ready', function() {
                // Start player
                player.play();
            });
        });
    }


    /*
     * Play next item
     */
    function next() {
        currentTrack += 1;
        play(currentTrack);
    }


    /*
     * Play previous item
     */
    function prev() {
        currentTrack -= 1;
        play(currentTrack);
    }


    /*
     * Stop player
     */
    function stop(reload) {
        if (player) {
            player.stop();
            player = null;
        }

        if (reload) {
            currentCodec = null;
        }
        currentTrack = 0;

        // Current track
        listBlock.find('a.music i.fa-play').removeClass('fa-play').addClass('fa-music');
        listBlock.find('a.music i.fa-pause').removeClass('fa-pause').addClass('fa-music');

        // Slider
        $$('.player-slider').css({width: 0});

        $$('.toolbar').addClass('toolbar-hidden');
    }

    /*
     * Toggle playback
     */
    function togglePlayback() {
        if (player) {
            player.togglePlayback();
            if (player.playing) {
                listBlock.find('a.music i.fa-pause').removeClass('fa-pause').addClass('fa-play');
            }
            else {
                listBlock.find('a.music i.fa-play').removeClass('fa-play').addClass('fa-pause');
            }
        }
        else {
            play();
        }
    }

    return {
        init: init,
        next: next,
        play: play,
        prev: prev,
        stop: stop
    };

}());



// Initialize application
var musicApp = new Framework7({
    animateNavBackIcon:true,
// SLOW!    material: true,
    precompileTemplates: true,
    pushState: true
});

// Export selectors engine
var $$ = Dom7;

var mainView = musicApp.addView('.view-main', {
});

musicApp.onPageInit('folders', function(page) {
    l.init(page, libraryOptions);
    l.refresh();
});

musicApp.onPageInit('player', function(page) {
    l.init(page, libraryOptions);
    p.init(page, playerOptions);

    l.refresh(function() { p.play(); });
});
