<form class="searchbar searchbar-init" data-search-in=".item-title" data-search-list=".library">
    <div class="searchbar-input">
        <input type="search" placeholder="Search" class=""><a class="searchbar-clear" href="#"></a>
    </div>
</form>
<div class="searchbar-overlay"></div>

<div class="list-block searchbar-not-found">
    <ul>
        <li class="item-content">
            <div class="item-inner">
                <div class="item-title">${_(u'Nothing found')}</div>
            </div>
        </li>
    </ul>
</div>

<div class="list-block searchbar-found library" data-url="${api_url}">
    <ul></ul>
</div>

<div class="content-block">
    <a href="${request.route_path('player')}" class="button button-big button-raised btn-play-all">${_(u'Play all items')}</a>
</div>
