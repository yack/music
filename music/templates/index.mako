<%inherit file="/site.mako" />
<div data-page="index" class="page main-page">
    <div class="page-content">
        <div class="list-block">
            <ul>
                <li>
                    <a class="item-link close-panel" href="${request.route_path('albums')}">
                        <div class="item-content">
                            <div class="item-inner">
                                <div class="item-title">${_(u'Albums')}</div>
                            </div>
                        </div>
                    </a>
                </li>
                <li>
                    <a class="item-link close-panel" href="${request.route_path('artists')}">
                        <div class="item-content">
                            <div class="item-inner">
                                <div class="item-title">${_(u'Artists')}</div>
                            </div>
                        </div>
                    </a>
                </li>
                <li>
                    <a class="item-link close-panel" href="${request.route_path('playlists')}">
                        <div class="item-content">
                            <div class="item-inner">
                                <div class="item-title">${_(u'Playlists')}</div>
                            </div>
                        </div>
                    </a>
                </li>
                <li>
                    <a class="item-link close-panel" href="${request.route_path('folders')}">
                        <div class="item-content">
                            <div class="item-inner">
                                <div class="item-title">${_(u'Folders')}</div>
                            </div>
                        </div>
                    </a>
                </li>
                <li>
                    <a class="item-link close-panel" href="${request.route_path('player')}">
                        <div class="item-content">
                            <div class="item-inner">
                                <div class="item-title">${_(u'Player')}</div>
                            </div>
                        </div>
                    </a>
                </li>
            </ul>
        </div> <!-- /.list-block -->
    </div><!-- /.page-content -->
</div><!-- /.page -->
