<%inherit file="/site.mako" />\
<div data-page="player" class="page">
    <div class="page-content">
        <div class="content-block">
            <div class="player-controls buttons-row">
                <a href="#" class="button button-big button-raised control-prev">${_('PREVIOUS')}</a>
                <a href="#" class="button button-big button-raised control-stop">${_('STOP')}</a>
                <a href="#" class="button button-big button-raised control-play">${_('PLAY/PAUSE')}</a>
                <a href="#" class="button button-big button-raised control-next">${_('NEXT')}</a>
            </div>
        </div><!-- /.content-block -->

        <div class="content-block list-block search-here searchbar-found library" data-url="${api_url}">
            <ul></ul>
        </div><!-- /.content-block -->

        <div class="content-block">
            <a href="${request.route_path('index')}" class="button button-big button-raised btn-clear-all">${_(u'Clear playlist')}</a>
        </div><!-- /.content-block -->
    </div><!-- /.page-content -->
</div><!-- /.page -->
