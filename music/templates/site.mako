<% route_name = request.matched_route.name if request.matched_route else None %>\
<!DOCTYPE html>
<html lang="${request.locale_name}">
<head>
    <!-- Required meta tags-->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, minimum-scale=1, user-scalable=no, minimal-ui">

    <!-- Fullscreen mode -->
    <meta name="mobile-web-app-capable" content="yes">
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="apple-mobile-web-app-status-bar-style" content="black">

    <title>Music player</title>

    <!-- Font Awesome -->
    <link href="${request.static_path('music:static/lib/font-awesome-4.4.0/css/font-awesome.min.css')}" rel="stylesheet" />

    <!-- sprintf -->
    <script src="${request.static_path('music:static/lib/sprintf.min.js')}"></script>

    <!-- Framework7 -->
    <link rel="stylesheet" href="${request.static_path('music:static/lib/framework7-1.2.0/css/framework7.material.min.css')}">
    <link rel="stylesheet" href="${request.static_path('music:static/lib/framework7-1.2.0/css/framework7.material.colors.min.css')}">

    <!-- Music -->
    <link rel="stylesheet" href="${request.static_path('music:static/css/music.css')}">

    <script>
var playerOptions = {
    codecs: {
        '.aac': "${request.static_path('music:static/lib/audiocogs/aac.js')}",
        '.alac': "${request.static_path('music:static/lib/audiocogs/alac.js')}",
        '.flac': "${request.static_path('music:static/lib/audiocogs/flac.js')}",
        '.m4a': "${request.static_path('music:static/lib/audiocogs/aac.js')}",
        '.mp3': "${request.static_path('music:static/lib/audiocogs/mp3.js')}",
        '.ogg': "${request.static_path('music:static/lib/audiocogs/vorbis.js')}"
    },
    library: "${request.route_path('library', subpath='')}",
    main: "${request.static_path('music:static/lib/audiocogs/aurora.js')}"
};

var libraryOptions = {
    library: "${request.route_path('library', subpath='')}",
    playlist: "${request.route_path('api.playlist')}"
};
    </script>
</head>
<body>
    <!-- Status bar overlay for full screen mode (PhoneGap) -->
    <div class="statusbar-overlay"></div>

    <!-- Panels overlay -->
    <div class="panel-overlay"></div>

    <!-- Left panel with reveal effect-->
    <div class="panel panel-left panel-reveal">
        <div class="view navbar-fixed" data-page="panel-left">
            <div class="pages">
                <div class="page" data-page="panel-left">
                    <div class="navbar">
                        <div class="navbar-inner">
                            <div class="center">${_(u'Menu')}</div>
                        </div>
                    </div>
                    <div class="page-content">
                        <div class="list-block">
                            <ul>
                                <li>
                                    <a class="item-link close-panel" href="${request.route_path('albums')}">
                                        <div class="item-content">
                                            <div class="item-inner">
                                                <div class="item-title">${_(u'Albums')}</div>
                                            </div>
                                        </div>
                                    </a>
                                </li>
                                <li>
                                    <a class="item-link close-panel" href="${request.route_path('artists')}">
                                        <div class="item-content">
                                            <div class="item-inner">
                                                <div class="item-title">${_(u'Artists')}</div>
                                            </div>
                                        </div>
                                    </a>
                                </li>
                                <li>
                                    <a class="item-link close-panel" href="${request.route_path('playlists')}">
                                        <div class="item-content">
                                            <div class="item-inner">
                                                <div class="item-title">${_(u'Playlists')}</div>
                                            </div>
                                        </div>
                                    </a>
                                </li>
                                <li>
                                    <a class="item-link close-panel" href="${request.route_path('folders')}">
                                        <div class="item-content">
                                            <div class="item-inner">
                                                <div class="item-title">${_(u'Folders')}</div>
                                            </div>
                                        </div>
                                    </a>
                                </li>
                                <li>
                                    <a class="item-link close-panel" href="${request.route_path('player')}">
                                        <div class="item-content">
                                            <div class="item-inner">
                                                <div class="item-title">${_(u'Player')}</div>
                                            </div>
                                        </div>
                                    </a>
                                </li>
                            </ul>
                        </div> <!-- /.list-block -->
                    </div> <!-- /.page-content -->
                </div> <!-- /.page -->
            </div> <!-- /.pages -->
        </div> <!-- /.view -->
    </div> <!-- /.panel -->

    <!-- Views -->
    <div class="views">
        <!-- Your main view, should have "view-main" class -->
        <div class="view view-main">
            <!-- Top Navbar-->
            <div class="navbar">
                <div class="navbar-inner">
                    <div class="left">
                        <a href="${request.route_path('index')}" class="link back"><i class="fa fa-chevron-left"> </i></a>
                    </div>
                    <!-- We need cool sliding animation on title element, so we have additional "sliding" class -->
                    <div class="center sliding">${_(u'Music')}</div>
                    <div class="right">
                        <!--
                            Right link contains only icon - additional "icon-only" class
                            Additional "open-panel" class tells app to open panel when we click on this link
                        -->
                        <a href="#" class="link icon-only open-panel"><i class="fa fa-bars"></i></a>
                    </div>
                </div>
            </div>

            <!-- Pages container, because we use fixed-through navbar and toolbar, it has additional appropriate classes -->
            <div class="pages navbar-fixed toolbar-through">
${self.body()}
            </div>

            <div class="toolbar toolbar-hidden">
                <div class="toolbar-inner">
                    <span class="current-track"></span>
                    <span class="elapsed-time">00:00</span>
                </div>
                <div class="player-slider"> </div>
            </div>
        </div><!-- /.view -->
    </div><!-- /.views -->

    <!-- Templates -->
    <script type="text/template7" id="libraryItemTemplate">
    <li>
        <a class="item-link {{type}}" href="{{link}}" data-index="{{index}}" data-name="{{name}}" data-path="{{path}}" data-extension="{{extension}}">
            <div class="item-content">
                <div class="item-media"><i class="fa fa-{{type}}"> </i></div>
                <div class="item-inner">
                    <div class="item-title">{{name}}</div>
                    {{#if extension}}
                    <div class="item-after extension"><span class="badge bg-bluegray">{{extension}}</span></div>
                    {{/if}}
                </div>
            </div>
        </a>
    </li>
    </script>

    <!-- Framework7 -->
    <script src="${request.static_path('music:static/lib/framework7-1.2.0/js/framework7.min.js')}"></script>

    <!-- Music -->
    <script src="${request.static_path('music:static/js/music.js')}"></script>
</body>
</html>
