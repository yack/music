# -*- coding: utf-8 -*-

# Music -- An HTML5 music player
# By: Cyril Lacoux <clacoux@ifeelgood.org>
#
# Copyright (C) 2015 Cyril Lacoux
# http://ifeelgood.org/music
#
# This file is part of Music.
#
# Music is free software; you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# Music is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import logging
import os

from pyramid.httpexceptions import HTTPForbidden
from pyramid.httpexceptions import HTTPInternalServerError
from pyramid.httpexceptions import HTTPNotFound
from pyramid.view import view_config
from pyramid.response import FileResponse

from pyramid_helpers.forms import validate

from music.forms import FolderForm
from music.forms import PlaylistForm


log = logging.getLogger(__name__)


#
# Front views
#
@view_config(route_name='index', renderer='/index.mako')
def index(request):
    return dict()


@view_config(route_name='albums', renderer='/albums.mako')
def albums(request):
    return dict(
        api_url=request.route_path('api.albums')
    )


@view_config(route_name='artists', renderer='/artists.mako')
def artists(request):
    return dict(
        api_url=request.route_path('api.artists')
    )


@view_config(route_name='folders', renderer='/folders.mako')
def folders(request):
    return dict(
        api_url=request.route_path('api.folders')
    )


@view_config(route_name='player', renderer='/player.mako')
def player(request):
    return dict(
        api_url=request.route_path('api.playlist')
    )


@view_config(route_name='playlists', renderer='/playlists.mako')
def playlists(request):
    return dict(
        api_url=request.route_path('api.playlists')
    )


@view_config(route_name='library')
def library(request):
    _ = request.translate

    # Get library directory
    registry = request.registry
    settings = registry.settings

    library_path = settings.get('library.directory')
    if library_path is None:
        raise HTTPInternalServerError(explanation=_('Unconfigured library, please contact system administrator'))

    fullpath = os.path.join(library_path, *request.subpath)
    if not os.path.isfile(fullpath):
        raise HTTPNotFound()

    if not os.access(fullpath, os.R_OK):
        raise HTTPForbidden(explanation=_('Not enough permissions to access requested file'))

    # Get valid extensions
    extensions = settings.get('library.extensions', '').split()

    root, extension = os.path.splitext(fullpath)
    if extension not in extensions:
        raise HTTPForbidden(explanation=_('File extension not allowed'))

    return FileResponse(fullpath)


#
# API views
#
@view_config(route_name='api.albums', renderer='json')
def api_albums(request):
    return dict()


@view_config(route_name='api.artists', renderer='json')
def api_artists(request):
    return dict()


@view_config(route_name='api.folders', renderer='json')
@validate('folder', FolderForm, method='get')
def api_folders(request):
    _ = request.translate
    form = request.forms['folder']

    # Prepare result
    result = dict(
        url=request.url,
        method=request.path,
        params=form.decoded,
        apiVersion='1.0',
        success=True,
    )

    # Get library directory
    registry = request.registry
    settings = registry.settings

    current_path = settings.get('library.directory')
    if current_path is None:
        result['message'] = _('Unconfigured library, please contact system administrator')
        result['success'] = False
        return result

    path = form.result['path']
    if path is not None:
        current_path = os.path.join(current_path, path.lstrip('/'))

    result['path'] = path

    if not os.path.isdir(current_path):
        result['message'] = _(u'Invalid path {0}').format(path)
        result['success'] = False
        return result

    if not os.access(current_path, os.R_OK | os.X_OK):
        result['message'] = _(u'Not enough permissions to access path {0}').format(path)
        result['success'] = False
        return result

    # Get valid extensions
    extensions = settings.get('library.extensions', '').split()

    # List files in path
    dirnames = []
    filenames = []
    for name in os.listdir(current_path):
        fullpath = os.path.join(current_path, name)
        if os.path.isdir(fullpath):
            dirnames.append(name)
        elif os.path.isfile(fullpath):
            filenames.append(name)

    result['items'] = []

    # Add dirnames
    dirnames.sort()
    result['items'].extend(
        dict(name=dirname, path=path, type='folder')
        for dirname in dirnames
    )

    # Add filenames (+check extension)
    filenames.sort()
    for filename in filenames:
        root, extension = os.path.splitext(filename)
        if extension in extensions:
            result['items'].append(dict(name=root, path=os.path.join(path, filename), type='music', extension=extension))

    return result


@view_config(route_name='api.playlists', renderer='json')
def api_playlists(request):
    return dict()


#
# Current playlist
#
@view_config(route_name='api.playlist', renderer='json', request_method='DELETE')
def api_playlist_delete(request):
    session = request.session

    # Prepare result
    result = dict(
        url=request.url,
        method=request.path,
        apiVersion='1.0',
        success=True,
    )

    # Reset playlist
    session['playlist'] = []
    return result


@view_config(route_name='api.playlist', renderer='json', request_method='GET')
def api_playlist_get(request):
    _ = request.translate
    session = request.session

    # Prepare result
    result = dict(
        url=request.url,
        method=request.path,
        apiVersion='1.0',
        success=True,
    )

    # Get library directory
    registry = request.registry
    settings = registry.settings

    current_path = settings.get('library.directory')
    if current_path is None:
        result['message'] = _('Unconfigured library, please contact system administrator')
        result['success'] = False
        return result

    result['items'] = session.get('playlist', [])
    return result


@view_config(route_name='api.playlist', renderer='json', request_method='POST')
@validate('playlist', PlaylistForm)
def api_playlist_post(request):
    _ = request.translate
    session = request.session
    form = request.forms['playlist']

    # Prepare result
    result = dict(
        url=request.url,
        method=request.path,
        params=form.decoded,
        apiVersion='1.0',
        success=True,
    )

    if form.errors:
        result['errors'] = form.errors
        result['message'] = _(u'Failed to update playlist')
        result['success'] = False
        return result

    # Get library directory
    registry = request.registry
    settings = registry.settings

    current_path = settings.get('library.directory')
    if current_path is None:
        result['message'] = _('Unconfigured library, please contact system administrator')
        result['success'] = False
        return result

    # Get valid extensions
    extensions = settings.get('library.extensions', '').split()

    playlist = session.setdefault('playlist', [])
    for path in form.result['items']:
        name = os.path.basename(path)
        root, extension = os.path.splitext(name)
        if extension not in extensions:
            continue
        playlist.append(dict(name=root, path=path, type='music', extension=extension))

    return result


@view_config(route_name='api.playlist', renderer='json', request_method='PUT')
def api_playlist_put(request):
    session = request.session

    # Reset playlist
    del session['playlist']
    return api_playlist_post(request)
